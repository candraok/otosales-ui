<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>ALL NEW ORDER</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,PERSONAL,KOMISI,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,PERSONAL,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,COMP3Y,M200,FULLCVR,PERSONAL,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,IEP,COMP3Y,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,IEP,COMP3Y,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG GO,IEP,COMP3Y,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG LEXUS,COMP3Y,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG LEXUS,COMP3Y,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG LEXUS,COMP3Y,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG SYARIAH,COMP3Y,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG SYARIAH,COMP3Y,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG SYARIAH,COMP3Y,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG SYARIAH,COMP3Y,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG SYARIAH,COMP3Y,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG SYARIAH,COMP3Y,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG SYARIAH,COMP3Y,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG SYARIAH,COMP3Y,M200,FULLCVR,PERSONAL,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG TI,COMP3Y,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/MULTI YEAR/INPROSPECT-NONGODIG TI,COMP3Y,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,COMP,M200,FULLCVR,PERSONAL,NOREK,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG GO,IEP,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG LEXUS,COMP,M200,FULLCVR,PERSONAL,NOREK</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,COMPANY,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,KOMISI,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG SYARIAH,COMP,M200,FULLCVR,PERSONAL,NOAPPADJ</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,COMPANY</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/OTOSALES/NEW ORDER/MVNONGODIG/SINGLE YEAR/INPROSPECT-NONGODIG TI,COMP,M200,FULLCVR,PERSONAL</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
