import com.keyword.GEN5
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
/*
def TestObject newTestObject(String locator){
		TestObject updatedTestObject = new TestObject("Grid")
		updatedTestObject.addProperty("xpath", ConditionType.EQUALS, locator)

		return updatedTestObject
	}

def getOneRowDatabase(String url, String dbname, int totalColumn, String queryTable) {
	GEN5.connectDB(url, dbname, "sa", "Password95")
	ArrayList columnData = new ArrayList()

	def Data = GEN5.executeQuery(queryTable)
	Data.next()

	int i
	for (i = 1 ; i <= totalColumn ; i++) {
		Object getData = Data.getObject(i)
		columnData.add(getData)
	}
	return columnData
}

def CompareFieldtoDatabase (ArrayList ObjRep, String url, String dbname, int totalColumn, String queryTable) {
	String FrameXpath = '/html/frameset/frame'
	WebUI.switchToDefaultContent()
	WebUI.switchToFrame(newTestObject(FrameXpath), 1)
	
	int count = ObjRep.size()
	ArrayList collectData = new ArrayList()
	
	int i
	for (i = 0 ; i < count ; i++) {
		collectData.add(WebUI.getAttribute(ObjRep[i], 'value', FailureHandling.STOP_ON_FAILURE))
	}
	
	int a
	ArrayList DBData = getOneRowDatabase(url, dbname, totalColumn, queryTable)
 
	for (a = 0 ; a < count ; a++) {
		if (collectData[a].trim() == DBData[a].trim()) {
			KeywordUtil.markPassed('Value \'' + collectData[a] + '\' from field, has same value in database ' + DBData[a] )
		} else {
			KeywordUtil.markWarning('Field Value = \''+ collectData + '\', value in database = \'' + DBData[a] + '\'')
		}
	}
	WebUI.switchToDefaultContent()
}
*/

List<TestObject> Obj = [findTestObject('Object Repository/test/combobox'),
	findTestObject('Object Repository/test/combobox2'),
		findTestObject('Object Repository/test/customer_name')]

GEN5.CompareFieldtoDatabase (Obj, '172.16.94.48', 'AABMobile', 10, 
	'Select * from AABMobile where PolicyNo = \'19078294322\'')



