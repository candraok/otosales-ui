import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.UI
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// Open Browser Chrome
//WebUI.openBrowser('https://www-qc.gardaoto.com/')

UI.AccessURL("Godig")

//Define Registration Number of vehicle
CustomKeywords.'general.database.connectLiTT'()

def getRegistrationNo = CustomKeywords.'general.database.executeQuery'('SELECT VALUE FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Registration Number\'')

getRegistrationNo.next()

String RegistrationNo = getRegistrationNo.getString('Value')

def Plate = CustomKeywords.'general.web.addRegistrationGodig'(Region, RegistrationNo)

GlobalVariable.RegistrationNo = Plate

// Action script
WebUI.delay(3)

WebUI.refresh()

WebUI.click(findTestObject('Object Repository/GODIG/Home/Open Search Vehicle'))

WebUI.setText(findTestObject('Object Repository/GODIG/Home/Input Search Vehicle'), vehicleName)

WebUI.sendKeys(findTestObject('Object Repository/GODIG/Home/Input Search Vehicle'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/GODIG/Home/Input No Plat'), Plate)

// Update Value in database
CustomKeywords.'general.database.connectLiTT'()

CustomKeywords.'general.database.execute'('UPDATE [LiTT].[dbo].[Otosales] SET Value = Value + 1 WHERE Parameters = \'Registration Number\'')

// Next Step
WebUI.click(findTestObject('Object Repository/GODIG/Home/Button Penawaran'))

