import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// List of Variables to get Value
//String NamaLengkap = WebUI.getText(findTestObject('Object Repository/GODIG/Step 2/Input Nama Lengkap'), FailureHandling.STOP_ON_FAILURE)

String TanggalLahir = WebUI.getText(findTestObject('Object Repository/GODIG/Step 2/Input Tanggal Lahir'), FailureHandling.STOP_ON_FAILURE)

//String Email = WebUI.getText(findTestObject('Object Repository/GODIG/Step 2/Input Email'), FailureHandling.STOP_ON_FAILURE)

//String PhoneNo = WebUI.getText(findTestObject('Object Repository/GODIG/Step 2/Input Phone No'), FailureHandling.STOP_ON_FAILURE)

// Variable to makes text uppercase
String JenisKelamin = Kelamin.toUpperCase()

// Variables to define upload file location
String uploadLocation = 'D:\\Repository\\Otosales\\Otosales - General\\Helper\\Picture'

// Wait for page loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/GODIG/Step 2/Input Nama Lengkap'), 3)

WebUI.waitForElementClickable(findTestObject('Object Repository/GODIG/Step 2/Input Nama Lengkap'), 3)

// Action script
/*if (!(NamaLengkap)) {
    WebUI.setText(findTestObject('Object Repository/GODIG/Step 2/Input Nama Lengkap'), 'Nama tidak tercantum')

    println('Nama tidak tercantum di step 2')
}*/

if (!(TanggalLahir)) {
    WebUI.click(findTestObject('Object Repository/GODIG/Step 2/Input Tanggal Lahir'))

    WebUI.click(findTestObject('Object Repository/GODIG/Step 2/Datepicker today'))
}

WebUI.click(findTestObject('Object Repository/GODIG/Step 2/Combobox Jenis Kelamin'))

WebUI.click(findTestObject('Object Repository/GODIG/Step 2/Combobox Jenis Kelamin - Option', [('sex') : JenisKelamin]))

WebUI.setText(findTestObject('Object Repository/GODIG/Step 2/Input Alamat'), Rumah)

WebUI.click(findTestObject('Object Repository/GODIG/Step 2/Combobox Kode Pos'))

WebUI.setText(findTestObject('Object Repository/GODIG/Step 2/Combobox Kode Pos - search'), Kodepos)

WebUI.sendKeys(findTestObject('Object Repository/GODIG/Step 2/Combobox Kode Pos - search'), Keys.chord(Keys.ENTER))

/*if (!Email) {
	WebUI.setText(findTestObject('Object Repository/GODIG/Step 2/Input Email'), 'Email tidak tercantum')
	
	println('Email tidak tercantum di step 2')
}*/

WebUI.setText(findTestObject('Object Repository/GODIG/Step 2/Input No KTP'), NoKTP)

/*if (!PhoneNo) {
	WebUI.setText(findTestObject('Object Repository/GODIG/Step 2/Input Phone No'), '0000000000')
}*/

if (DeliveryAdd) {
	WebUI.click(findTestObject('Object Repository/GODIG/Step 2/Checkbox Delivery Address'))
} else {
	WebUI.setText(findTestObject('Object Repository/GODIG/Step 2/Input Alamat Pengiriman'), AlamatPengiriman)
	
	WebUI.click(findTestObject('Object Repository/GODIG/Step 2/Combobox Kodepos Pengiriman'))
	
	WebUI.setText(findTestObject('Object Repository/GODIG/Step 2/Combobox Kode Pos - search'), KodeposPengiriman)
	
	WebUI.sendKeys(findTestObject('Object Repository/GODIG/Step 2/Combobox Kode Pos - search'), Keys.chord(Keys.ENTER))
}

// Upload KTP
WebUI.click(findTestObject('Object Repository/GODIG/Step 2/Upload KTP'))

WebUI.delay(1)

CustomKeywords.'general.web.uploadImage1'(uploadLocation, 'sir john kotelawala.jpeg')

// Upload STNK
WebUI.click(findTestObject('Object Repository/GODIG/Step 2/Upload STNK'))

WebUI.delay(2)

CustomKeywords.'general.web.uploadImage1'(uploadLocation, 'stephen chow.jpeg')


// Click on Captcha if available
/*def captcha = WebUI.waitForElementPresent(findTestObject('Object Repository/GODIG/Step 2/Checkbox Captcha'), 0)

if (captcha) {
	WebUI.click(findTestObject('Object Repository/GODIG/Step 2/Checkbox Captcha'))
}*/

// Proceed to Next Step
WebUI.click(findTestObject('Object Repository/GODIG/Step 2/Button Next'))

WebUI.delay(2)










