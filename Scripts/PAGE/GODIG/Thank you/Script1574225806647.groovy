import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String AllText = WebUI.getText(findTestObject('Object Repository/GODIG/Thank you/Text field'), FailureHandling.STOP_ON_FAILURE)

String TextOrd = WebUI.getText(findTestObject('Object Repository/GODIG/Thank you/Text Order No'), FailureHandling.STOP_ON_FAILURE)

String prefix = AllText.substring(127, 132)

String VANumber = AllText.substring(133, 144)

String Harga = AllText.substring(157, 166)

String Price = Harga.replace(",", "")

String OrderNo = TextOrd.split(" : ")

println ('Bank Code is ' + prefix)

println ('Your Virtual Account Number is ' + VANumber)

println ('The price is ' + Price)

println ('Your Order Number is ' + OrderNo)

GlobalVariable.GodigPrefix = prefix

GlobalVariable.GodigPrice = Price

GlobalVariable.GodigOrderNo = OrderNo

GlobalVariable.GodigVANumber = VANumber