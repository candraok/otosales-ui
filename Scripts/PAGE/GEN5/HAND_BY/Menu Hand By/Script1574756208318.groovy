import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.ArrayList

WebDriver Driver = DriverFactory.getWebDriver()

WebElement table = Driver.findElement(By.xpath("//table[@id=\"dtHandBy\"]//tr"))

List<WebElement> Row =  table.findElements(By.tagName("th"))

String[] title = ['No', 'Vehicle', 'Coverage', 'Customer Name', 'Nomor HP', 'Status Otosales', 'Marketing']

List<String> collsName = new ArrayList()

//String HandBy = WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/HAND_BY/Text Judul'), 0)

String HandBy = WebUI.getText(findTestObject('Object Repository/GEN5/HAND_BY/Text Judul'), FailureHandling.STOP_ON_FAILURE)

String DisMark = WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/HAND_BY/Field Marketing Disable'), 0)

String DisMob = WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/HAND_BY/Field Mobile No Disable'), 0)

String SeaParam = WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/HAND_BY/Notifikasi search parameter'), 0)

String ResParam = WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/HAND_BY/Notifikasi Reset Parameter'), 0)

String SelEmp = WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/HAND_BY/Notifikasi Select Empty'), 0)

String CloPage = WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/HAND_BY/Notifikasi Close Page'), 0)

println(HandBy)

// Check Page title
if (HandBy == 'HandBy GO DIGITAL') {
	println ('PASSED - Header Name is Menu HandBy GO DIGITAL')
} else {
	println ('FAILED - Header Name is Incorrect')
}

// Check field Marketing is disable by default
if (DisMark) {
	println ('PASSED - Field Marketing is Disable')
} else {
	println ('FAILED - Field Marketing must be Disable')
}

// Check field Mobile No is disable by default
if (DisMob) {
	println ('PASSED - Field Mobile No is Disable')
} else {
	println ('FAILED - Field Mobile No must be Disable')
}

// Check when check Marketing's Radio Button
WebUI.click(findTestObject('Object Repository/GEN5/HAND_BY/Radio Marketing'), FailureHandling.STOP_ON_FAILURE)

if (DisMark) {
	println ('FAILED - Field Marketing still Disable')
} else {
	println ('PASSED - Field Marketing is enable after check Marketing\'s Radio Button')
}

if (DisMob) {
	println ('PASSED - Field Mobile No is Disable after check Marketing\'s Radio Button')
} else {
	println ('FAILED - Field Mobile No must be Disable after check Marketing\'s Radio Button')
}

// Check when check Mobile No's Radio Button
WebUI.click(findTestObject('Object Repository/GEN5/HAND_BY/Radio Mobile No'), FailureHandling.STOP_ON_FAILURE)

if (DisMark) {
	println ('PASSED - Field Marketing is Disable after check Mobile No\'s Radio Button')
} else {
	println ('FAILED - Field Marketing must be Disable after check Mobile No\'s Radio Button')
}

if (DisMob) {
	println ('FAILED - Field Mobile No still Disable')
} else {
	println ('PASSED - Field Mobile No must is enable after check Mobile No\'s Radio Button')
}

// Check notification of input parameter when click it without fill any parameters
WebUI.click(findTestObject('Object Repository/GEN5/HAND_BY/Button Search'), FailureHandling.STOP_ON_FAILURE)

if (SeaParam) {
	println ('PASSED - Notification Please Input Search Parameter is Displayed')
	
	WebUI.click(findTestObject('Object Repository/GEN5/HAND_BY/Button Close Pop Up'), FailureHandling.STOP_ON_FAILURE)
} else {
	println ('FAILED - Notification Please Input Search Paramenter is NOT Displayed')
}

// Check notification of Reset Button
WebUI.click(findTestObject('Object Repository/GEN5/HAND_BY/Button Reset'), FailureHandling.STOP_ON_FAILURE)

if (ResParam) {
	println ('PASSED - Notification Reset Parameter is Displayed')
	
	WebUI.click(findTestObject('Object Repository/GEN5/HAND_BY/Button Close Pop Up'), FailureHandling.STOP_ON_FAILURE)
} else {
	println ('FAILED - Notification Reset Paramenter is NOT Displayed')
}

// Check the table header's name
for (i = 0 ; i < Row.size() ; i++) {
	List<WebElement> header = Row[i].findElements(By.tagName("span"))
	
	String write = header[0].getText()
	
	//println (header)
	println (write + ' - ' + title[i])
	if (write == title[i]) {
		println ('PASSED - The Coloumn Name is ' + write)
	} else {
		println ('FAILED - The Coloumn Name should be ' + title[i] +  ' instead of ' + write )
	}
}

// Check Notification if click it without choose any search result
WebUI.click(findTestObject('Object Repository/GEN5/HAND_BY/Button Select'), FailureHandling.STOP_ON_FAILURE)

if (SelEmp) {
	println ('PASSED - Notification Select without choose search result is Displayed')
	
	WebUI.click(findTestObject('Object Repository/GEN5/HAND_BY/Button Close Pop Up'), FailureHandling.STOP_ON_FAILURE)
} else {
	println ('FAILED - Notification Select without choose search result is NOT Displayed')
}

// Check Notification if close button pressed
WebUI.click(findTestObject('Object Repository/GEN5/HAND_BY/Button Close'), FailureHandling.STOP_ON_FAILURE)

if (CloPage) {
	println ('PASSED - Notification Close Edit Hand By Page is Displayed')
	
	WebUI.click(findTestObject('Object Repository/GEN5/HAND_BY/Button Close Pop Up'), FailureHandling.STOP_ON_FAILURE)
} else {
	println ('FAILED - Notification Close Edit Hand By Page is NOT Displayed')
}




