import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.waitForPageLoad(GlobalVariable.LongestDelay)

GEN5.ProcessingCommand()

WebUI.delay(1)

WebUI.click(findTestObject('GEN5/TASKLIST SA/Dashboard/Combobox Search By'))

WebUI.delay(1)

WebUI.click(findTestObject('GEN5/TASKLIST SA/Dashboard/Choose Search By', [('search') : SearchBy]), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('GEN5/TASKLIST SA/Dashboard/Input Parameter'), Parameter, FailureHandling.STOP_ON_FAILURE)

//WebUI.sendKeys(findTestObject('GEN5/TASKLIST SA/Dashboard/Input Parameter'), Keys.chord(Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('GEN5/TASKLIST SA/Dashboard/Button Search'))

GEN5.ProcessingCommand()

WebUI.delay(2)

WebUI.click(findTestObject('GEN5/TASKLIST SA/Dashboard/Choose Search Result'))

WebUI.delay(2)

WebUI.click(findTestObject('GEN5/TASKLIST SA/Dashboard/Button Select'))

