import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// Write Plate, Chassis dan Engine based on Respected Region
CustomKeywords.'general.database.connectLiTT'()

def getRegistrationNo = CustomKeywords.'general.database.executeQuery'('SELECT VALUE FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Registration Number\'')

def getChassisNo = CustomKeywords.'general.database.executeQuery'('SELECT VALUE FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Chassis Number\'')

def getEngineNo = CustomKeywords.'general.database.executeQuery'('SELECT VALUE FROM [LiTT].[dbo].[Otosales] WHERE Parameters = \'Engine Number\'')

getRegistrationNo.next()

getChassisNo.next()

getEngineNo.next()

String RegistrationNo = getRegistrationNo.getString('Value')

String ChassisNo = getChassisNo.getString('Value')

String EngineNo = getEngineNo.getString('Value')

GlobalVariable.RegistrationNo = Plat + RegistrationNo + 'ATM'

GlobalVariable.ChassisNo = Plat + ChassisNo + 'ATM'

GlobalVariable.EngineNo = Plat + EngineNo + 'ATM'

String WrongPlat = PlatWrong + RegistrationNo + 'ATM'

String jenisKelamin = Kelamin.toUpperCase()

// File Upload Location
String uploadLocation = 'D:\\Repository\\Otosales\\Otosales - General\\Helper\\Picture'

// Vehicle Data
WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Text Title'), 3)

String Header = WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Text Title'), 3)

// Action Script
if (Header) {
    println('Data Kendaraan & Perluasan Jaminan')
} else {
    println('Title not Found')
}

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox Pertanggungan'))

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox Pertanggungan - Option', [('cover') : Coverage]))

WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Kendaraan'), Vehicle, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Kendaraan'), Keys.chord(Keys.ENTER))

if (CekPlat) {
    if (Plat != 'B' || Plat != 'F' || Plat != 'D' || Plat != 'L' || PlatWrong != 'B' || PlatWrong != 'F' || PlatWrong != 'D' || PlatWrong != 'L') {
        WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Plat'), WrongPlat, FailureHandling.STOP_ON_FAILURE)

        WebUI.sendKeys(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Plat'), Keys.chord(Keys.TAB))

        WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Text Warning'), 3, FailureHandling.STOP_ON_FAILURE)

        WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Button Close Warning'))

        WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Plat'), GlobalVariable.RegistrationNo, 
            FailureHandling.STOP_ON_FAILURE)

        WebUI.sendKeys(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Plat'), Keys.chord(Keys.TAB))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'), 
            10, FailureHandling.STOP_ON_FAILURE)

        println('PASSED - Check Incorrect Plat')
    } else {
        WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Plat'), GlobalVariable.RegistrationNo, 
            FailureHandling.STOP_ON_FAILURE)

        WebUI.sendKeys(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Plat'), Keys.chord(Keys.TAB))

        WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'), 
            10, FailureHandling.STOP_ON_FAILURE)

        println('PASSED - Plat is Correct')
    }
} else {
    WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Plat'), GlobalVariable.RegistrationNo, 
        FailureHandling.STOP_ON_FAILURE)

    WebUI.sendKeys(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Plat'), Keys.chord(Keys.TAB))

    WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'), 10, 
        FailureHandling.STOP_ON_FAILURE)
}

WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Warna Kendaraan'), Color, FailureHandling.STOP_ON_FAILURE)

if (PADR) {
    WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox PA Driver'))

	WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox PA Driver - Option', [('driver') : DrAmount]))
	
    WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'), 10, 
        FailureHandling.STOP_ON_FAILURE)
}

if (PAPS) {
    WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox PA Passanger'))

    WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox PA Passanger - Option', [('Passanger') : PassAmount]))

    WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'), 10, 
        FailureHandling.STOP_ON_FAILURE)
}

if (TJH) {
    WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox TJH'))

    WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox TJH - Option', [('TJHAM') : TJHAmount]))

    WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'), 10, 
        FailureHandling.STOP_ON_FAILURE)
}

if (SRCC) {
    WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Checkbox SRCC'))
}

if (Terorism) {
    WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Checkbox Terorisme'))
}

WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input No Rangka'), GlobalVariable.ChassisNo, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'),
	10, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input No Mesin'), GlobalVariable.EngineNo, FailureHandling.STOP_ON_FAILURE)

// Survey Section
WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox Kota Survey'))

WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'),
	10, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox Kota Survey'))

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox Kota Survey - Option', [('city') : SurveyCity]), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox Lokasi Survey'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox Lokasi Survey - Choose'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Menu Survey Date'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Menu Survey Date - Choose'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox Waktu Survey'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox Waktu Survey - Choose'), FailureHandling.STOP_ON_FAILURE)

// Personal Data
WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Nama Tertanggung'), Nama, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Tanggal Lahir'))

for (i = 0; i < 10; i++) {
    WebUI.sendKeys(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Tanggal Lahir'), Keys.chord(Keys.BACK_SPACE))
}

WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Tanggal Lahir'), TanggalLahir, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox Jenis Kelamin'))

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Combobox Jenis Kelamin - Option', [('sex') : jenisKelamin]))

WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Email'), Email, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input No HP'), Phone, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input No KTP'), KTP, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Alamat'), Alamat, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Kodepos'), Kodepos, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Kodepos'), Keys.chord(Keys.ENTER))

if (AlamatSama) {
	WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Checkbox Alamat Pengiriman'))
	
} else {
	WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Alamat Pengiriman'), AlamatPengiriman, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Kodepos Pengiriman'), Kodepos2, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.sendKeys(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Kodepos'), Keys.chord(Keys.ENTER))
}

// Upload Image KTP
WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Upload KTP'))

WebUI.delay(1)

CustomKeywords.'general.web.uploadImage1'(uploadLocation, 'sir john kotelawala.jpeg')

// Upload Image STNK
WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Upload STNK'))

WebUI.delay(1)

CustomKeywords.'general.web.uploadImage1'(uploadLocation, 'stephen chow.jpeg')

// Fill Marketing ID
WebUI.setText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Marketing ID'), Marketing, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Input Marketing ID'), Keys.chord(Keys.ENTER))

// Update Value in database
CustomKeywords.'general.database.connectLiTT'()

CustomKeywords.'general.database.execute'('UPDATE [LiTT].[dbo].[Otosales] SET Value = Value + 1 WHERE Parameters = \'Registration Number\'')

CustomKeywords.'general.database.execute'('UPDATE [LiTT].[dbo].[Otosales] SET Value = Value + 1 WHERE Parameters = \'Chassis Number\'')

CustomKeywords.'general.database.execute'('UPDATE [LiTT].[dbo].[Otosales] SET Value = Value + 1 WHERE Parameters = \'Engine Number\'')

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Button Submit'))
