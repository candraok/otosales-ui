import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Summary/Button Generate Link'), 3)

WebUI.waitForElementClickable(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Summary/Button Generate Link'), 3)

String OrderNo = WebUI.getText(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Summary/Text Order No'))

println(OrderNo)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Summary/Button Generate Link'))

WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'),
	10, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Summary/Button Send Email'))

WebUI.waitForElementVisible(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Summary/Pop Up Information'), 5, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Summary/Button Close Pop up'))

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Summary/Button Send SMS'))

WebUI.waitForElementVisible(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Summary/Pop Up Information'), 5, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Summary/Button Close Pop up'))

if (process == 'GEN5') {
	WebUI.click(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Summary/Button Buy Now'))
	
	WebUI.waitForElementNotPresent(findTestObject('Object Repository/GEN5/CREATE URL NEW GODIG/Processing Command'),
		10, FailureHandling.STOP_ON_FAILURE)
} else if (process == 'GODIG') {
	CustomKeywords.'general.database.connectDB'('172.16.94.48', 'Asuransiastra', 'sa', 'Password95')
	
	def getShortURL = CustomKeywords.'general.database.executeQuery'('SELECT * FROM GODigital.LinkPayment WHERE PolicyOrderNo = \'' + OrderNo + '\'')
	
	getShortURL.next()
	
	String ShortURL = getShortURL.getString('ShortenURL')
	
	GlobalVariable.ShortenURL = ShortURL
	
	WebUI.callTestCase(findTestCase('PAGE/GODIG/Launch Short URL'),
		[('Shorten'): GlobalVariable.ShortenURL],
		FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('PAGE/GODIG/Step 3'),
		[('Payment'): 'VIRTUAL ACCOUNT'],
		FailureHandling.STOP_ON_FAILURE)
	
}




