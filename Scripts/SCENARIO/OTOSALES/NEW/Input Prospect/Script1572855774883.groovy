import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/LOGIN/Side Menu-Switch'), 
	[('menu') : 'Input Prospect'], 				// Write the side menu
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/INPUT PROSPECT/1.Prospect Info'), 
	[('MV'): 'MV GODIG',						// Option MV GODIG or MV NON GODIG
	('isCompany'): false, 						// True if the order is company, false to personal
	('prospectName'): 'Automate Account' , 
	('phone1'): '082127670262' , 
	('phone2'): '081290154328' , 
	('email'): 'cok@cak.ep' , 
	('CashDealer'): false , 					// true if the order is Cash Dealer, false to otherwise
	('DealerNameFull'): 'AUTO 2000 CILANDAK JKT' , 
	('SalesNameFull'): 'CANDRA NUGROHO'], 
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/INPUT PROSPECT/2.Vehicle Info'), 
	[('VehicleName'): 'XPANDER 2018 ULTIMATE', 	// Write the vehicle name and year
	('Usage'): 'PRIBADI' ,						// Vehicle usage
	('Region'): 'Wilayah 2'], 					// Vehicle area
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/INPUT PROSPECT/3.Cover'), 
	[('ProductType'): 'Garda Oto', 				// Choose Garda Oto, Garda Oto Syariah, Toyota Insurance or Lexus Insurance
	('ProductCode'): 'GWU50' ,					// Choose Product Code base on Product Type
	('Coverage'): 'Comprehensive 1 Year' , 		// Choose Basic Coverage
	('TJH'): '25,000,000' , 					// TJH Amount 0 for No TJH and higher than 0 to choose TJH (coma separator)
	('Terrorism'): true , 						// True to Choose Terrorism and false to skip it
	('Driver'): '20,000,000', 					// Driver Amount 0 for No PA Driver & higher than 0 to otherwise (coma separator)
	('Passenger'): '10,000,000' , 				// Passanger Amount 0 for No PA Pass & higher than 0 to otherwise (coma separator)
	('Accessory') : '7000000'], 				// Accessories Amount 0 for No Acc & higher than 0 to otherwise (coma separator)
	FailureHandling.STOP_ON_FAILURE)

