import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('PAGE/OTOSALES/NEW/TASKLIST/3.FollowUp'),
	[('Reason'): 'Send to SA',							// Choose Send to SA, Kirim Penawaran or Kelengkapan Dokumen
	('Notes'): 'Contacted',								// Choose Contacted, Not Connected or Not Contacted
	('Receiver'): 'Sir Cok Kotelawala' ,				// Write Follow Up person's name 
	('Remarks'): 'Created by Katalon Studio', 			// Free text
	('ProspectName'): GlobalVariable.ProspectName ], 	
	FailureHandling.STOP_ON_FAILURE)