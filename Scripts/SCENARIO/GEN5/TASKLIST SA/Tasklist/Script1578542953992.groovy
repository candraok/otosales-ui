import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('PAGE/GEN5/LOGIN/Login'),
	[('username'): 'DOC',
	('password'): 'P@ssw0rd'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/GEN5/HOME/Side Menu'),
	[('SideMenu'): 'Task List SA'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/GEN5/TASKLIST SA/Dashboard'),
	[('SearchBy') : 'Order No', 						// Choose Order No, Chasis No or Engine No
	('Parameter') : GlobalVariable.OtosalesOrderNo ],						// GlobalVariable.OtosalesOrderNo
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('PAGE/GEN5/TASKLIST SA/Tasklist SA'),
	[('changePolicyHolder'): false,							// true for change policy Holder
	('SearchParam'): 'Name', 								// Search Parameter (Code, Name, NPWP, Birth Date, etc)
	('PolicyHolderName'): 'Sir Cok Kotelawala' ,			// New Policy Holder Name
	('NameOnPolicy'): 'Default' ,							// Choose Default, New, DefaultQQNew, NewQQDefault or Other
	('EmailDelivery'): 'Customer' ,							// Choose Customer, Policy Holder or Both
	('editVehicle'): true ,									// true to open vehicle detail
	('Choice'): 'Submit' , 									// Choose 'Submit' or 'Send Back to AO'
	('Remarks'): 'Automatic Remarks to Send Back to AO' ], 	// Free Text
	FailureHandling.STOP_ON_FAILURE)
