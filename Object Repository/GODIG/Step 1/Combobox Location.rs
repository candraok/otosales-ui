<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Location</name>
   <tag></tag>
   <elementGuidId>0d8b801b-02f0-41e9-b188-23ed69ba0cf8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;LOKASI SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyLocation&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;LOKASI SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyLocation&quot;]</value>
   </webElementProperties>
</WebElementEntity>
