<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Waktu Survey - Option</name>
   <tag></tag>
   <elementGuidId>09fc456b-14a7-4648-aa4d-85e75df69032</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;WAKTU SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyTimeIn&quot;]//option[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;WAKTU SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyTimeIn&quot;]//option[2]</value>
   </webElementProperties>
</WebElementEntity>
