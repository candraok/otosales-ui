<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input TJH</name>
   <tag></tag>
   <elementGuidId>8ebd8665-3f17-417d-a28b-d91a30077c61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),&quot;(TJH)&quot;)]//following-sibling::span//select[@id=&quot;thirdPartyPrice&quot;]//option[text()=&quot;${TJHPrice}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;(TJH)&quot;)]//following-sibling::span//select[@id=&quot;thirdPartyPrice&quot;]//option[text()=&quot;${TJHPrice}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
