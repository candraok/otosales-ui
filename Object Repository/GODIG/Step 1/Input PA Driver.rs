<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input PA Driver</name>
   <tag></tag>
   <elementGuidId>08809d6a-db36-41c7-ab0d-e4fbec6e3679</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@class=&quot;checkboxInput&quot; and @id=&quot;paDriverChecked&quot;]//following-sibling::span//input[@id=&quot;paDriverPrice&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@class=&quot;checkboxInput&quot; and @id=&quot;paDriverChecked&quot;]//following-sibling::span//input[@id=&quot;paDriverPrice&quot;]</value>
   </webElementProperties>
</WebElementEntity>
