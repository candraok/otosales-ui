<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Datepicker Survey Date - Today</name>
   <tag></tag>
   <elementGuidId>cf1bd6a0-6e3f-47d6-9f33-0b33ad741278</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;xdsoft_calendar&quot;]//td[contains(@class,&quot;today true&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;xdsoft_calendar&quot;]//td[contains(@class,&quot;today true&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
