<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox PA Driver</name>
   <tag></tag>
   <elementGuidId>4096b740-bc5f-4ced-b93b-c07e959bc7aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@class=&quot;checkboxInput&quot; and @id=&quot;paDriverChecked&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@class=&quot;checkboxInput&quot; and @id=&quot;paDriverChecked&quot;]</value>
   </webElementProperties>
</WebElementEntity>
