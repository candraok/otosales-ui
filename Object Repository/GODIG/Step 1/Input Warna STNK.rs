<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Warna STNK</name>
   <tag></tag>
   <elementGuidId>242d371c-c164-41d0-af8c-8ba9ba3a62ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;WARNA KENDARAAN SESUAI STNK *&quot;]//following-sibling::span//input[@id=&quot;WarnaStnk&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;WARNA KENDARAAN SESUAI STNK *&quot;]//following-sibling::span//input[@id=&quot;WarnaStnk&quot;]</value>
   </webElementProperties>
</WebElementEntity>
