<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Waktu Survey</name>
   <tag></tag>
   <elementGuidId>ca13a05f-e70c-40d2-a584-92e4167166b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;WAKTU SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyTimeIn&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;WAKTU SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyTimeIn&quot;]</value>
   </webElementProperties>
</WebElementEntity>
