<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Location - Option</name>
   <tag></tag>
   <elementGuidId>e6644107-db9e-47d9-8371-3929599fda65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;LOKASI SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyLocation&quot;]//option[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;LOKASI SURVEI *&quot;]//following-sibling::span//select[@id=&quot;SurveyLocation&quot;]//option[2]</value>
   </webElementProperties>
</WebElementEntity>
