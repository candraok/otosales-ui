<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input No Rangka</name>
   <tag></tag>
   <elementGuidId>1b3b67cc-878e-489f-af4a-71a5162df304</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;NO RANGKA *&quot;]//following-sibling::span//input[@id=&quot;NoRangka&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;NO RANGKA *&quot;]//following-sibling::span//input[@id=&quot;NoRangka&quot;]</value>
   </webElementProperties>
</WebElementEntity>
