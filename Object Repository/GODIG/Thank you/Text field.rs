<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text field</name>
   <tag></tag>
   <elementGuidId>fb03116a-7050-4f41-a869-135f14442ee6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id=&quot;PaymentResponseContainer&quot;]//p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id=&quot;PaymentResponseContainer&quot;]//p</value>
   </webElementProperties>
</WebElementEntity>
