<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox Captcha</name>
   <tag></tag>
   <elementGuidId>0609b211-3027-4f5e-8e2e-779b431f4a8e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;recaptcha-checkbox-borderAnimation&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;recaptcha-checkbox-borderAnimation&quot;]</value>
   </webElementProperties>
</WebElementEntity>
