<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Jenis Kelamin - Option</name>
   <tag></tag>
   <elementGuidId>f004a96c-17d1-461f-acdb-96ab042a1048</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@id=&quot;JenisKelamin&quot;]//option[text()=&quot;${sex}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@id=&quot;JenisKelamin&quot;]//option[text()=&quot;${sex}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
