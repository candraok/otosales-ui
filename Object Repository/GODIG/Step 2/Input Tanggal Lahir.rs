<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Tanggal Lahir</name>
   <tag></tag>
   <elementGuidId>56e25d8a-f4b8-497e-a2f8-3e8aeb255ba4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;TANGGAL LAHIR *&quot;]//following-sibling::span//input[@id=&quot;TanggalLahir&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;TANGGAL LAHIR *&quot;]//following-sibling::span//input[@id=&quot;TanggalLahir&quot;]</value>
   </webElementProperties>
</WebElementEntity>
