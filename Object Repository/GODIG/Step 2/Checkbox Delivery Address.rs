<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox Delivery Address</name>
   <tag></tag>
   <elementGuidId>baa4f8a8-e5a2-45e4-b392-bcfe35aeb49c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id=&quot;DeliveryAddressCheckBox&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id=&quot;DeliveryAddressCheckBox&quot;]</value>
   </webElementProperties>
</WebElementEntity>
