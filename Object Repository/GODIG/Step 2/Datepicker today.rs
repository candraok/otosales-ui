<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Datepicker today</name>
   <tag></tag>
   <elementGuidId>4aa6230c-e783-4744-9385-b260b97b11f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[@class=&quot;xdsoft_calendar&quot;]//td[contains(@class,&quot;today&quot;)])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@class=&quot;xdsoft_calendar&quot;]//td[contains(@class,&quot;today&quot;)])[1]</value>
   </webElementProperties>
</WebElementEntity>
