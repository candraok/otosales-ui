<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Email</name>
   <tag></tag>
   <elementGuidId>5334693e-b2ac-4c0c-8b0f-1a36f034ef6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id=&quot;EmailQuotation&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id=&quot;EmailQuotation&quot;]</value>
   </webElementProperties>
</WebElementEntity>
