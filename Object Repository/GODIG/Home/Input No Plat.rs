<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input No Plat</name>
   <tag></tag>
   <elementGuidId>c960a0ab-d86b-404f-a67a-a4df996f8e90</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(@class,&quot;NoPlat&quot;)]//input[@id=&quot;NoPlat&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(@class,&quot;NoPlat&quot;)]//input[@id=&quot;NoPlat&quot;]</value>
   </webElementProperties>
</WebElementEntity>
