<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Search Vehicle</name>
   <tag></tag>
   <elementGuidId>ecf7ebd5-b5ab-439b-bf6d-469e03a72a42</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(@class,&quot;search--dropdown&quot;)]//input[@type=&quot;search&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(@class,&quot;search--dropdown&quot;)]//input[@type=&quot;search&quot;]</value>
   </webElementProperties>
</WebElementEntity>
