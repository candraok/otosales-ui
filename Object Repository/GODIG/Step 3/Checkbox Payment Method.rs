<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox Payment Method</name>
   <tag></tag>
   <elementGuidId>6f4446e3-6b04-4b9f-b1bd-b5c0a55ca470</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,&quot;paymentBankSelectionContainer&quot;)]//span[text()=&quot;${bank}&quot;]//parent::div//input[@type=&quot;radio&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,&quot;paymentBankSelectionContainer&quot;)]//span[text()=&quot;${bank}&quot;]//parent::div//input[@type=&quot;radio&quot;]</value>
   </webElementProperties>
</WebElementEntity>
