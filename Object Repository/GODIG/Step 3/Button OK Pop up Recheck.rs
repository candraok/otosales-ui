<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button OK Pop up Recheck</name>
   <tag></tag>
   <elementGuidId>b4a3f9d7-cb27-43ac-bde5-56ebf3864a60</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@type=&quot;button&quot; and text()=&quot;OK&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@type=&quot;button&quot; and text()=&quot;OK&quot;]</value>
   </webElementProperties>
</WebElementEntity>
