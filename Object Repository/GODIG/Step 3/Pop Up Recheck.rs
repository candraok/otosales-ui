<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pop Up Recheck</name>
   <tag></tag>
   <elementGuidId>4d32406d-8230-4aa4-9a9a-9feb063a44dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(text(),&quot;Pastikan Anda sudah setuju&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),&quot;Pastikan Anda sudah setuju&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
