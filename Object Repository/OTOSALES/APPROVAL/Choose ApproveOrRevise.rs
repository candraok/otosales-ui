<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose ApproveOrRevise</name>
   <tag></tag>
   <elementGuidId>d7797fa2-b8a2-497a-ad22-c3b172754d65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;panel-body&quot;]//button[text()=&quot;${choose}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;panel-body&quot;]//button[text()=&quot;${choose}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
