<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Send Remarks</name>
   <tag></tag>
   <elementGuidId>be695556-d597-4b83-91b5-56814867393b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//p[text()=&quot;Remarks&quot;]//parent::div//preceding-sibling::div//i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//p[text()=&quot;Remarks&quot;]//parent::div//preceding-sibling::div//i</value>
   </webElementProperties>
</WebElementEntity>
