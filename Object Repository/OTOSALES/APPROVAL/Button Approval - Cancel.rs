<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Approval - Cancel</name>
   <tag></tag>
   <elementGuidId>b18ade83-d737-4924-ad9a-938555c7fab0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h5[text()=&quot;Order Approval&quot;]//ancestor::div[2]//button[text()=&quot;Reject&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h5[text()=&quot;Order Approval&quot;]//ancestor::div[2]//button[text()=&quot;Reject&quot;]</value>
   </webElementProperties>
</WebElementEntity>
