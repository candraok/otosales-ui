<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Approval</name>
   <tag></tag>
   <elementGuidId>d06ab90e-f04a-4e89-82c1-693cc325d905</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;panel-body&quot;]/h4/strong[text()=&quot;${name}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;panel-body&quot;]/h4/strong[text()=&quot;${name}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
