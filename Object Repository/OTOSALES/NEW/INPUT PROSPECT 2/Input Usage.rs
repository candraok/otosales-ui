<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Usage</name>
   <tag></tag>
   <elementGuidId>59015332-3f86-4f5c-8171-ce8fc1d13ef3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;USAGE * &quot;]//parent::div//input[@type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;USAGE * &quot;]//parent::div//input[@type=&quot;text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
