<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Salesman Dealer</name>
   <tag></tag>
   <elementGuidId>6e658759-14f4-4fb9-ad4d-4f58ea41672d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;text&quot; and @placeholder=&quot;Select salesman dealer name&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;text&quot; and @placeholder=&quot;Select salesman dealer name&quot;]</value>
   </webElementProperties>
</WebElementEntity>
