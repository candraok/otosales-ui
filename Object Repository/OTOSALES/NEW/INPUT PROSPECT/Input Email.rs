<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Email</name>
   <tag></tag>
   <elementGuidId>07b2d8e7-5c01-4627-9074-3bb101e6dc1b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;email&quot; and @name=&quot;Email1&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;email&quot; and @name=&quot;Email1&quot;]</value>
   </webElementProperties>
</WebElementEntity>
