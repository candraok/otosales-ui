<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Phone 2</name>
   <tag></tag>
   <elementGuidId>553137fb-9a97-4046-bf27-9912500d4a5d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),&quot;PHONE NUMBER 2&quot;)]//parent::div//input[@class=&quot;form-control&quot; and @type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;PHONE NUMBER 2&quot;)]//parent::div//input[@class=&quot;form-control&quot; and @type=&quot;text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
