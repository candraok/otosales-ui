<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Conf Pop Up Yes</name>
   <tag></tag>
   <elementGuidId>da146a70-93fd-45c3-9e7c-0fe6162b38d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-footer&quot;]//button[text()=&quot;Yes&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-footer&quot;]//button[text()=&quot;Yes&quot;]</value>
   </webElementProperties>
</WebElementEntity>
