<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search Salesman Dealer</name>
   <tag></tag>
   <elementGuidId>5da5a974-20b0-49fc-b0fa-087afbb60b37</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type='text' and @placeholder='Ketik Select salesman dealer name' and @name='search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type='text' and @placeholder='Ketik Select salesman dealer name' and @name='search']</value>
   </webElementProperties>
</WebElementEntity>
