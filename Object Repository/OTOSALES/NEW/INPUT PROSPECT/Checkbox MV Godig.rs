<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox MV Godig</name>
   <tag></tag>
   <elementGuidId>75333d43-ee3d-47f3-995d-5533cd650ef5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()='MV GO DIGITAL']//preceding-sibling::input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()='MV GO DIGITAL']//preceding-sibling::input</value>
   </webElementProperties>
</WebElementEntity>
