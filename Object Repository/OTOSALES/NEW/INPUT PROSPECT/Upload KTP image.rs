<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Upload KTP image</name>
   <tag></tag>
   <elementGuidId>b426258d-73b2-4a0e-b583-20c1658af5e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'KTP')]//parent::div//img[@class='contentimg']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'KTP')]//parent::div//img[@class='contentimg']</value>
   </webElementProperties>
</WebElementEntity>
