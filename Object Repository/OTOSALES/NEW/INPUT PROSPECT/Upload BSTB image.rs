<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Upload BSTB image</name>
   <tag></tag>
   <elementGuidId>f36ca5c9-10ef-4b6e-b452-bf06bc3ff569</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'BSTB')]//parent::div//following-sibling::div//img[@class='contentimg']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'BSTB')]//parent::div//following-sibling::div//img[@class='contentimg']</value>
   </webElementProperties>
</WebElementEntity>
