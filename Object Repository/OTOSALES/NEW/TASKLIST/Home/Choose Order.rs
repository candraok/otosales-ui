<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Order</name>
   <tag></tag>
   <elementGuidId>e0d355f1-129d-4d4e-ba1a-321a1f3ba0c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;panel-body panel-body-list&quot;]//div[contains(text(),&quot;ew&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;panel-body panel-body-list&quot;]//div[contains(text(),&quot;ew&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
