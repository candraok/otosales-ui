<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combo Box Search Engine</name>
   <tag></tag>
   <elementGuidId>12f4c2ac-675c-447a-be61-16e774025a91</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@class=&quot;form-control&quot; and @name=&quot;searchType&quot;]//option[@value=&quot;enn&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@class=&quot;form-control&quot; and @name=&quot;searchType&quot;]//option[@value=&quot;enn&quot;]</value>
   </webElementProperties>
</WebElementEntity>
