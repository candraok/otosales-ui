<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Search</name>
   <tag></tag>
   <elementGuidId>26009a97-63e0-4480-84ac-35f71ede46a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li//a[contains(@class,&quot;fa-search&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li//a[contains(@class,&quot;fa-search&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
