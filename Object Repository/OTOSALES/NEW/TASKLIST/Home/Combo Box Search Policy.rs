<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combo Box Search Policy</name>
   <tag></tag>
   <elementGuidId>23e219f4-64b6-4233-be22-da071d08bc5b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@class=&quot;form-control&quot; and @name=&quot;searchType&quot;]//option[@value=&quot;pcn&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@class=&quot;form-control&quot; and @name=&quot;searchType&quot;]//option[@value=&quot;pcn&quot;]</value>
   </webElementProperties>
</WebElementEntity>
