<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combo Box Search Chasis</name>
   <tag></tag>
   <elementGuidId>75981677-2049-48a7-b098-5d7b59fd310e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@class=&quot;form-control&quot; and @name=&quot;searchType&quot;]//option[@value=&quot;csn&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@class=&quot;form-control&quot; and @name=&quot;searchType&quot;]//option[@value=&quot;csn&quot;]</value>
   </webElementProperties>
</WebElementEntity>
