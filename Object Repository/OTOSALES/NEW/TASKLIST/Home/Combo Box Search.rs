<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combo Box Search</name>
   <tag></tag>
   <elementGuidId>722ff601-f3a0-4cd9-aa8a-21e2e0d03961</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@class=&quot;form-control&quot; and @name=&quot;searchType&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@class=&quot;form-control&quot; and @name=&quot;searchType&quot;]</value>
   </webElementProperties>
</WebElementEntity>
