<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Search Result by Name</name>
   <tag></tag>
   <elementGuidId>02df353e-f580-43e4-93c6-b7f0819796dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[contains(@class,&quot;panel-body-list&quot;) and @name=&quot;sampleList&quot;]//div[contains(@id,&quot;textCustPolNo&quot;)])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[contains(@class,&quot;panel-body-list&quot;) and @name=&quot;sampleList&quot;]//div[contains(@id,&quot;textCustPolNo&quot;)])[1]</value>
   </webElementProperties>
</WebElementEntity>
