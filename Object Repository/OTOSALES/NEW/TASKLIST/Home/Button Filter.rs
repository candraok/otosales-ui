<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Filter</name>
   <tag></tag>
   <elementGuidId>14b9a18e-3fa4-406f-830b-18258b0f21dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li//a[contains(@class,&quot;fa-filter&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li//a[contains(@class,&quot;fa-filter&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
