<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Field Search</name>
   <tag></tag>
   <elementGuidId>67c15cd8-8b73-416d-8680-80e92fa43573</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//i[contains(@class,&quot;fa-search&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//i[contains(@class,&quot;fa-search&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
