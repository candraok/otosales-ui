<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Follow Up Reason</name>
   <tag></tag>
   <elementGuidId>d41580bf-49c2-4872-9e28-c3e74f768188</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@placeholder=&quot;Choose follow up reason&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@placeholder=&quot;Choose follow up reason&quot;]</value>
   </webElementProperties>
</WebElementEntity>
