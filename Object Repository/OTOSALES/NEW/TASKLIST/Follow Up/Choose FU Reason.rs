<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose FU Reason</name>
   <tag></tag>
   <elementGuidId>e695553e-fa62-467d-a29d-4ef8f7575ccd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;${option}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;${option}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
