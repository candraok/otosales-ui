<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Vehicle Color</name>
   <tag></tag>
   <elementGuidId>3ff69357-5be8-4e7c-a499-bd505d124f65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name=&quot;vehiclecolor&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name=&quot;vehiclecolor&quot;]</value>
   </webElementProperties>
</WebElementEntity>
