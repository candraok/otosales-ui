<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input PIC Hp</name>
   <tag></tag>
   <elementGuidId>a4e5a0bc-297e-4e5f-9103-b75c7378e1d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Nomor Handphone PIC&quot;]//following-sibling::div//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Nomor Handphone PIC&quot;]//following-sibling::div//input</value>
   </webElementProperties>
</WebElementEntity>
