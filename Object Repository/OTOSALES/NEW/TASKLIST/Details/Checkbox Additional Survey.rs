<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox Additional Survey</name>
   <tag></tag>
   <elementGuidId>ee56b4c1-621c-4f51-b564-adefedd36b7f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;SUDAH SURVEY ADDITIONAL ORDER&quot;]//preceding-sibling::input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;SUDAH SURVEY ADDITIONAL ORDER&quot;]//preceding-sibling::input</value>
   </webElementProperties>
</WebElementEntity>
