<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Customer Birthday</name>
   <tag></tag>
   <elementGuidId>53e544d6-22c3-46fe-bb02-f4000e5dbf19</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Tanggal Lahir&quot;]//following-sibling::div//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Tanggal Lahir&quot;]//following-sibling::div//input</value>
   </webElementProperties>
</WebElementEntity>
