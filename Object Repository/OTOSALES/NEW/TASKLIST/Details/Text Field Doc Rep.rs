<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text Field Doc Rep</name>
   <tag></tag>
   <elementGuidId>fac5e222-4ba7-48af-bac6-0950c0184d40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Amount Rep&quot;]//following-sibling::div//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Amount Rep&quot;]//following-sibling::div//input</value>
   </webElementProperties>
</WebElementEntity>
