<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox NSA Skip Survey</name>
   <tag></tag>
   <elementGuidId>d44e4834-3b89-451f-bbe5-c283d465179c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;NSA SKIP SURVEY&quot;]//preceding-sibling::input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;NSA SKIP SURVEY&quot;]//preceding-sibling::input</value>
   </webElementProperties>
</WebElementEntity>
