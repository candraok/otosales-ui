<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Others Kodepos</name>
   <tag></tag>
   <elementGuidId>1f614f33-1fb8-4bc0-86d3-ea9260a64946</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[text()=&quot;Kode Pos&quot;]//following-sibling::div//input)[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[text()=&quot;Kode Pos&quot;]//following-sibling::div//input)[2]</value>
   </webElementProperties>
</WebElementEntity>
