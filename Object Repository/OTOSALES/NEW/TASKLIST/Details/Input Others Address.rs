<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Others Address</name>
   <tag></tag>
   <elementGuidId>053da237-5614-4b0d-a43e-92f90437e2f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Alamat&quot;]//following-sibling::div//input[@name=&quot;policyaddress&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Alamat&quot;]//following-sibling::div//input[@name=&quot;policyaddress&quot;]</value>
   </webElementProperties>
</WebElementEntity>
