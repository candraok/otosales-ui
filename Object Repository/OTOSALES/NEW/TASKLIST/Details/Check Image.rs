<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Check Image</name>
   <tag></tag>
   <elementGuidId>f6c8d1c6-d9bb-420a-a0fe-3f1ee1af5990</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//center[contains(text(),&quot;${image}&quot;)]//preceding-sibling::div//img[contains(@src,&quot;otosales&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//center[contains(text(),&quot;${image}&quot;)]//preceding-sibling::div//img[contains(@src,&quot;otosales&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
