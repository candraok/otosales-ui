<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Alamat Pengiriman</name>
   <tag></tag>
   <elementGuidId>0c6fc964-34bb-49f4-a173-934ebe1ab821</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Alamat&quot;]//following-sibling::div//input[@placeholder=&quot;Pilih Garda Center&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Alamat&quot;]//following-sibling::div//input[@placeholder=&quot;Pilih Garda Center&quot;]</value>
   </webElementProperties>
</WebElementEntity>
