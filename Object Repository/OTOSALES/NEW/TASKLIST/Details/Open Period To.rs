<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Open Period To</name>
   <tag></tag>
   <elementGuidId>96ca8cc2-a96b-445b-a29b-58ed2ab67ec1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Period To&quot;]//following-sibling::div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Period To&quot;]//following-sibling::div/input</value>
   </webElementProperties>
</WebElementEntity>
