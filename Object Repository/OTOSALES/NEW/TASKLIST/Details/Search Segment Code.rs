<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search Segment Code</name>
   <tag></tag>
   <elementGuidId>7d4c4d16-d19b-4b14-a56b-89313964f6e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@placeholder=&quot;Pilih Segment Code&quot; and @name=&quot;search&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@placeholder=&quot;Pilih Segment Code&quot; and @name=&quot;search&quot;]</value>
   </webElementProperties>
</WebElementEntity>
