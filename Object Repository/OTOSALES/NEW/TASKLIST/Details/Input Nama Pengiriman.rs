<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Nama Pengiriman</name>
   <tag></tag>
   <elementGuidId>5ca5a4b9-a658-4e64-81d0-d4961617b199</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Nama&quot;]//following-sibling::div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Nama&quot;]//following-sibling::div/input</value>
   </webElementProperties>
</WebElementEntity>
