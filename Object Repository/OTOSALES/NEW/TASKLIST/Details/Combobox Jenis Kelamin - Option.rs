<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Jenis Kelamin - Option</name>
   <tag></tag>
   <elementGuidId>b6a9d40b-952d-4f01-bcf7-1db1f455278e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@name=&quot;personalJK&quot;]//span[text()=&quot;${sex}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@name=&quot;personalJK&quot;]//span[text()=&quot;${sex}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
