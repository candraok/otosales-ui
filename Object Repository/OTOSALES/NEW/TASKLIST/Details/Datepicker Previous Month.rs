<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Datepicker Previous Month</name>
   <tag></tag>
   <elementGuidId>c8bfc106-dfcb-45e1-8019-c6a8bce577ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@type=&quot;button&quot; and text()=&quot;Previous Month&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@type=&quot;button&quot; and text()=&quot;Previous Month&quot;]</value>
   </webElementProperties>
</WebElementEntity>
