<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Footer Menu</name>
   <tag></tag>
   <elementGuidId>07cd60a3-1ec4-4fdb-8387-f3384dbdad2b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;root&quot;]/div/div/div/div[2]/div/div[4]/div[13]/div/div[2]/span[text()=&quot;${menu}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;root&quot;]/div/div/div/div[2]/div/div[4]/div[13]/div/div[2]/span[text()=&quot;${menu}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
