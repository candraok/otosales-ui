<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox Need Survey</name>
   <tag></tag>
   <elementGuidId>70d7e42c-923f-47ab-8618-0779c0117c28</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;NEED SURVEY&quot;]//preceding-sibling::input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;NEED SURVEY&quot;]//preceding-sibling::input</value>
   </webElementProperties>
</WebElementEntity>
