<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DatePicker Open Year</name>
   <tag></tag>
   <elementGuidId>e0164990-e1a9-4753-8f72-3f44d0b7cc9c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;react-datepicker__year-read-view&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;react-datepicker__year-read-view&quot;]</value>
   </webElementProperties>
</WebElementEntity>
