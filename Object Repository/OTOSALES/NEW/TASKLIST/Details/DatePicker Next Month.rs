<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DatePicker Next Month</name>
   <tag></tag>
   <elementGuidId>d1c19ff6-c2d1-4f84-ad3e-a3c7e3b43787</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@type=&quot;button&quot; and text()=&quot;Next Month&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@type=&quot;button&quot; and text()=&quot;Next Month&quot;]</value>
   </webElementProperties>
</WebElementEntity>
