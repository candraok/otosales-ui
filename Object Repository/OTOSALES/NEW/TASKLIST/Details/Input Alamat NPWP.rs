<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Alamat NPWP</name>
   <tag></tag>
   <elementGuidId>185d50ce-bbd8-49f9-bf7d-e3d50a3aeafc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Alamat NPWP&quot;]//following-sibling::div//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Alamat NPWP&quot;]//following-sibling::div//input</value>
   </webElementProperties>
</WebElementEntity>
