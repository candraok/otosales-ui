<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search Coverage</name>
   <tag></tag>
   <elementGuidId>85ea4740-6896-474e-8e7c-8d7894e99899</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name=&quot;search&quot; and @placeholder=&quot;Pilih Basic Cover&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@name=&quot;search&quot; and @placeholder=&quot;Pilih Basic Cover&quot;]</value>
   </webElementProperties>
</WebElementEntity>
