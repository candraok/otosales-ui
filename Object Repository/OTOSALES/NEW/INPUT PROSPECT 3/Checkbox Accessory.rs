<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox Accessory</name>
   <tag></tag>
   <elementGuidId>4be70ead-9567-4dcc-ace9-ff3a5b3cfc6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label//input[@type=&quot;checkbox&quot; and @name=&quot;IsACCESSChecked&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label//input[@type=&quot;checkbox&quot; and @name=&quot;IsACCESSChecked&quot;]</value>
   </webElementProperties>
</WebElementEntity>
