<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Basic Cover</name>
   <tag></tag>
   <elementGuidId>5349c528-ce38-4cbd-aaa8-906997796791</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class=&quot;smalltext&quot; and text()=&quot;${key3}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class=&quot;smalltext&quot; and text()=&quot;${key3}&quot;]</value>
   </webElementProperties>
</WebElementEntity>
