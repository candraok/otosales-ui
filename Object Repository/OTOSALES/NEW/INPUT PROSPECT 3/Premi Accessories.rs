<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Premi Accessories</name>
   <tag></tag>
   <elementGuidId>149b854d-b949-45c2-96a8-59a28ac21296</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),&quot;ACCESSORY&quot;)]//parent::div//parent::div//following-sibling::div//p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;ACCESSORY&quot;)]//parent::div//parent::div//following-sibling::div//p</value>
   </webElementProperties>
</WebElementEntity>
