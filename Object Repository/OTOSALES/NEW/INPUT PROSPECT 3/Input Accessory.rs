<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Accessory</name>
   <tag></tag>
   <elementGuidId>d4e0a0fa-b5d9-4665-b801-4502fac7e237</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Access SI&quot;]//following-sibling::div//input[@type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Access SI&quot;]//following-sibling::div//input[@type=&quot;text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
