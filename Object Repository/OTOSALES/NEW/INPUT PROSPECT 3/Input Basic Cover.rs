<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Basic Cover</name>
   <tag></tag>
   <elementGuidId>38c5a722-f3a1-4dbd-a9e8-7c027f570e9c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;BASIC COVER * &quot;]//parent::div//input[@placeholder=&quot;Select basic cover&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;BASIC COVER * &quot;]//parent::div//input[@placeholder=&quot;Select basic cover&quot;]</value>
   </webElementProperties>
</WebElementEntity>
