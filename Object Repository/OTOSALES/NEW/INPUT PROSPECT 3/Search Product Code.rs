<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search Product Code</name>
   <tag></tag>
   <elementGuidId>6aa0ecc0-9925-41fa-b545-dc6913d5e263</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@placeholder=&quot;Ketik Select product code&quot; and @name=&quot;search&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@placeholder=&quot;Ketik Select product code&quot; and @name=&quot;search&quot;]</value>
   </webElementProperties>
</WebElementEntity>
