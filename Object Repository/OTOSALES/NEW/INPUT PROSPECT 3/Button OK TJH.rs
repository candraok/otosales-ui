<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button OK TJH</name>
   <tag></tag>
   <elementGuidId>b05a35dd-c11a-4898-94de-750b90de5380</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class=&quot; btn btn-sm btn-info form-control&quot; and text()=&quot;OK&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class=&quot; btn btn-sm btn-info form-control&quot; and text()=&quot;OK&quot;]</value>
   </webElementProperties>
</WebElementEntity>
