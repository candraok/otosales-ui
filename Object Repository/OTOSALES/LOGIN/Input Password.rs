<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Password</name>
   <tag></tag>
   <elementGuidId>1c50af71-433c-4b19-8e24-1ab931bf69a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@placeholder=&quot;Enter password&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@placeholder=&quot;Enter password&quot;]</value>
   </webElementProperties>
</WebElementEntity>
