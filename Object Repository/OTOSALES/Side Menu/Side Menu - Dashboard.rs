<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Side Menu - Dashboard</name>
   <tag></tag>
   <elementGuidId>5e202c4c-cdb9-412d-88c2-1807136255c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//aside//a[@href=&quot;/dashboard&quot;])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//aside//a[@href=&quot;/dashboard&quot;])[2]</value>
   </webElementProperties>
</WebElementEntity>
