<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Side Menu - Tasklist</name>
   <tag></tag>
   <elementGuidId>bc25dc30-5fe8-466b-bd6c-4c5e65f70b9b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[contains(text(),'Task List')])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[contains(text(),'Task List')])[2]</value>
   </webElementProperties>
</WebElementEntity>
