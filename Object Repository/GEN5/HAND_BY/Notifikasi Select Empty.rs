<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Notifikasi Select Empty</name>
   <tag></tag>
   <elementGuidId>63c78fdd-7fd2-4e4d-a731-a881c52c8fe5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[contains(text(),&quot;Please select data first!&quot;)]//ancestor::div)[1][@aria-hidden=&quot;false&quot; and contains(@style,&quot;block&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[contains(text(),&quot;Please select data first!&quot;)]//ancestor::div)[1][@aria-hidden=&quot;false&quot; and contains(@style,&quot;block&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
