<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Field Mobile No Disable</name>
   <tag></tag>
   <elementGuidId>f952e683-1a24-4201-92d2-6758d6fa6759</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//b[contains(text(),&quot;Mobile No&quot;)]//parent::span//parent::div//following-sibling::div//input[@disabled]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//b[contains(text(),&quot;Mobile No&quot;)]//parent::span//parent::div//following-sibling::div//input[@disabled]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
