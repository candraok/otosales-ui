<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Notifikasi search parameter</name>
   <tag></tag>
   <elementGuidId>447edec5-c6ea-49d2-b82b-7d9973e833d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[contains(text(),&quot;Please input search parameter!&quot;)]//ancestor::div)[1][@aria-hidden=&quot;false&quot; and contains(@style,&quot;block&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[contains(text(),&quot;Please input search parameter!&quot;)]//ancestor::div)[1][@aria-hidden=&quot;false&quot; and contains(@style,&quot;block&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
