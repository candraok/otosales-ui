<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Notifikasi Reset Parameter</name>
   <tag></tag>
   <elementGuidId>672c46fa-7724-4c65-8289-89a629c168b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[contains(text(),&quot;reset search parameter?&quot;)]//ancestor::div)[1][@aria-hidden=&quot;false&quot; and contains(@style,&quot;block&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[contains(text(),&quot;reset search parameter?&quot;)]//ancestor::div)[1][@aria-hidden=&quot;false&quot; and contains(@style,&quot;block&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
