<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Field Marketing Disable</name>
   <tag></tag>
   <elementGuidId>41b42973-0630-4b5a-a8c5-3e4b1a2998c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//b[contains(text(),&quot;Marketing&quot;)]//parent::span//parent::div//following-sibling::div//span[@role=&quot;combobox&quot; and @tabindex=&quot;-1&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//b[contains(text(),&quot;Marketing&quot;)]//parent::span//parent::div//following-sibling::div//span[@role=&quot;combobox&quot; and @tabindex=&quot;-1&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
