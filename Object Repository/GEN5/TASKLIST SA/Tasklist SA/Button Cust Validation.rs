<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Cust Validation</name>
   <tag></tag>
   <elementGuidId>fb330e48-87a1-429b-80b8-8009209fe63b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3//ancestor::div[1]//button[text()=&quot;Update&quot;][count(. | //*[@ref_element = 'Object Repository/GEN5/Frame Set']) = count(//*[@ref_element = 'Object Repository/GEN5/Frame Set'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h3//ancestor::div[1]//button[text()=&quot;Update&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
