<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Next Personal Customer</name>
   <tag></tag>
   <elementGuidId>a76e50db-581d-4cdc-bace-f7c1ebe2c147</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3[text()=&quot;Personal Customer&quot;]//ancestor::div[1]//button[text()=&quot;Next&quot;][count(. | //*[@ref_element = 'Object Repository/GEN5/Frame Set']) = count(//*[@ref_element = 'Object Repository/GEN5/Frame Set'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h3[text()=&quot;Personal Customer&quot;]//ancestor::div[1]//button[text()=&quot;Next&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
