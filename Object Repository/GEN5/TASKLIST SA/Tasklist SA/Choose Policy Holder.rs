<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose Policy Holder</name>
   <tag></tag>
   <elementGuidId>3e350e2a-c806-48fc-99ac-b90a803fb695</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3[text()=&quot;Policy Holder&quot;]//ancestor::div[2]//table//tbody//tr[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h3[text()=&quot;Policy Holder&quot;]//ancestor::div[2]//table//tbody//tr[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
