<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Submit</name>
   <tag></tag>
   <elementGuidId>059816c5-c59d-4bd4-b74f-489f5188e403</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3[contains(text(),&quot;Order Summary&quot;)]//parent::div//Button[text()=&quot;${continue}&quot;][count(. | //*[@ref_element = 'Object Repository/GEN5/Frame Set']) = count(//*[@ref_element = 'Object Repository/GEN5/Frame Set'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h3[contains(text(),&quot;Order Summary&quot;)]//parent::div//Button[text()=&quot;${continue}&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
