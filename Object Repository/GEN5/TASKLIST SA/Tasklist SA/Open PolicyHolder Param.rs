<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Open PolicyHolder Param</name>
   <tag></tag>
   <elementGuidId>87231289-105c-4bc7-a492-cfc5f3743f25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()=&quot;Search By&quot;]//parent::label//following-sibling::div//span[text()=&quot;Code&quot;]//following-sibling::span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()=&quot;Search By&quot;]//parent::label//following-sibling::div//span[text()=&quot;Code&quot;]//following-sibling::span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
