<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>html</name>
   <tag></tag>
   <elementGuidId>c6f11558-9fc9-4079-9de9-2c30ee653c2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/frameset/ancestor::html[count(. | //*[@ref_element = 'Object Repository/GEN5/Frame Set']) = count(//*[@ref_element = 'Object Repository/GEN5/Frame Set'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/frameset/ancestor::html</value>
   </webElementProperties>
</WebElementEntity>
