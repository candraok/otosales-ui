<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Next Object</name>
   <tag></tag>
   <elementGuidId>add72e80-1951-4ee2-ae8e-aa14120fc703</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//h3[text()=&quot;Object&quot;]//parent::div//button[text()=&quot;Next&quot;])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//h3[text()=&quot;Object&quot;]//parent::div//button[text()=&quot;Next&quot;])[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
