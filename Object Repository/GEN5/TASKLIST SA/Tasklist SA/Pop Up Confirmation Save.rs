<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pop Up Confirmation Save</name>
   <tag></tag>
   <elementGuidId>6b724c1f-0eb3-4fc4-b2fe-cd5169f1604f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3[contains(text(),&quot;Confirmation Save&quot;)]//ancestor::div[2]//button[text()=&quot;Yes&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h3[contains(text(),&quot;Confirmation Save&quot;)]//ancestor::div[2]//button[text()=&quot;Yes&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
