<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text Warning</name>
   <tag></tag>
   <elementGuidId>d8e044b4-452d-4a35-a3b4-a0b53c21575c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3[text()=&quot;Warning&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h3[text()=&quot;Warning&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
