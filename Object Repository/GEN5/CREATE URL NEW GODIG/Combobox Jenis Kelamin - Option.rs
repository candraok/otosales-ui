<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox Jenis Kelamin - Option</name>
   <tag></tag>
   <elementGuidId>4b5feb17-b59c-44dc-b9c8-faa6e2e9e426</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[text()=&quot;Jenis Kelamin&quot;]//parent::label//following-sibling::div//button)[1]//following-sibling::div/button[text()=&quot;${sex}&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[text()=&quot;Jenis Kelamin&quot;]//parent::label//following-sibling::div//button)[1]//following-sibling::div/button[text()=&quot;${sex}&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
