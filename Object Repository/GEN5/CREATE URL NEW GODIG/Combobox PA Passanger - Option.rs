<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combobox PA Passanger - Option</name>
   <tag></tag>
   <elementGuidId>961639bb-e00d-44c2-9bdc-af6289b564d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//span[text()=&quot;Kecelakaan Diri Penumpang&quot;]//ancestor::a2is-textbox-dc//following-sibling::a2is-combo-nc//button)[1]//following-sibling::div//button[contains(text(),&quot;${Passanger}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//span[text()=&quot;Kecelakaan Diri Penumpang&quot;]//ancestor::a2is-textbox-dc//following-sibling::a2is-combo-nc//button)[1]//following-sibling::div//button[contains(text(),&quot;${Passanger}&quot;)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
