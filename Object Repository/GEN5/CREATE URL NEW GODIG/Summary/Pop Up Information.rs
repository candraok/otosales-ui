<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pop Up Information</name>
   <tag></tag>
   <elementGuidId>59639cfd-0a48-4200-92e7-af14308d3ab1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3[text()=&quot;Information&quot;]//parent::div//parent::div//parent::div//parent::div[@aria-hidden=&quot;false&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h3[text()=&quot;Information&quot;]//parent::div//parent::div//parent::div//parent::div[@aria-hidden=&quot;false&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
