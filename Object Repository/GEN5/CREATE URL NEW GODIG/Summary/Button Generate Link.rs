<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Generate Link</name>
   <tag></tag>
   <elementGuidId>9d53464c-c350-42bd-b1aa-50c951745357</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[text()=&quot;GENERATE LINK&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()=&quot;GENERATE LINK&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
