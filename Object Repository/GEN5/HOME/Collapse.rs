<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Collapse</name>
   <tag></tag>
   <elementGuidId>f362041f-13fe-40ab-8093-6c256cac1709</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[text()=&quot;${menus}&quot;]//parent::div//parent::div[@aria-expanded=&quot;true&quot;][count(. | //*[@ref_element = 'Object Repository/GEN5/Frame Set']) = count(//*[@ref_element = 'Object Repository/GEN5/Frame Set'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()=&quot;${menus}&quot;]//parent::div//parent::div[@aria-expanded=&quot;true&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/GEN5/Frame Set</value>
   </webElementProperties>
</WebElementEntity>
