package com.otosurvey

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import internal.GlobalVariable as strGlbVar
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement
import java.sql.PreparedStatement
import java.sql.Driver
import java.sql.Connection
import java.sql.Array
import sun.management.counter.StringCounter

public class DBQueryUtility {

	/*
	 @Keyword
	 def dbAddPanel(String idLK){
	 (new com.otosurvey.DBConnection().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))
	 def dataAddInfo = "SELECT ID_Add_Panel, dbo.OTOSURVEY_ADD_PANEL_LK.ID_LK, spnPanelObject, spnPanelCoverage, spnPropertyName, spnPropertyName_2, spnWorkingType, bWorkingTypeLostPanel, bWorkingTypeStickerNo, capDocLoop FROM dbo.OTOSURVEY_ADD_PANEL_LK INNER JOIN dbo.OTOSURVEY_LK ON dbo.OTOSURVEY_ADD_PANEL_LK.ID_LK = dbo.OTOSURVEY_LK.ID_LK WHERE dbo.OTOSURVEY_ADD_PANEL_LK.ID_LK ='"+idLK+"'"
	 ResultSet info = (new com.otosurvey.DBConnection().executeQuery(dataAddInfo))
	 while(info.next()){
	 strGlbVar.intIdAddPanel_LK.add(info.getString(2))
	 strGlbVar.strPanelObject_LK.add(info.getString(3))
	 strGlbVar.strPanelCoverage_LK.add(info.getString(4))
	 strGlbVar.strPropertyName_LK.add(info.getString(5))
	 strGlbVar.strPropertyName_2_LK.add(info.getString(6))
	 strGlbVar.strWorkingType_LK.add(info.getString(7))
	 strGlbVar.bWorkingTypeLostPanel_LK.add(info.getString(8))
	 strGlbVar.bWorkingTypeStickerNo_LK.add(info.getString(9))
	 strGlbVar.bCapDocLoop_LK.add(info.getString(10))
	 }
	 }
	 */

	/*
	 @Keyword
	 def dbOtoLK(){
	 (new com.otosurvey.DBConnection().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))
	 def data = "SELECT TOP 1 * FROM OTOSURVEY_LK WHERE bIsRun_LK='0'"
	 (new com.otosurvey.DBConnection().executeQuery(data))
	 }
	 @Keyword
	 def dbOtoSPK(){
	 (new com.otosurvey.DBConnection().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))
	 def data = "SELECT TOP 1 * FROM OTOSURVEY_SPK WHERE bIsRun_LK='0'"
	 (new com.otosurvey.DBConnection().executeQuery(data))
	 }
	 @Keyword
	 def updateFlagRunLK(String idLK){
	 String queryString = "UPDATE OTOSURVEY_LK SET bIsRun_LK = 1 WHERE ID_LK = '"+idLK+"'"
	 Statement stm = com.otosurvey.DBConnection.connectDB('172.16.94.48', 'litt', 'sa', 'Password95').createStatement()
	 boolean result = stm.execute(queryString)
	 return result
	 }
	 @Keyword
	 def updateFlagRunSPK(String idSPK){
	 (new com.otosurvey.DBConnection().connectDB('172.16.94.48', 'litt', 'sa', 'Password95'))
	 def data = "UPDATE OTOSURVEY_LK SET bIsRun_LK = 1 WHERE ID_LK = '"+idSPK+"'"
	 (new com.otosurvey.DBConnection().executeQuery(data))
	 }
	 */
}
