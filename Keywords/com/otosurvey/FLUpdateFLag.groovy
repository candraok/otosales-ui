package com.otosurvey

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Driver;

import internal.GlobalVariable

public class FLUpdateFLag {

	private static Connection connection = null;
	@Keyword
	def connectDB(String server, String dbname, String username, String password){
		//String ip = "172.16.94.48"
		//String db = "litt"
		//String us = "sa"
		//String ps = "Password95"
		//String po = "1433"
		//jdbc:sqlserver://localhost:1433;databaseName=DB_NAME
		//String url = "jdbc:sqlserver://"+a+"/"+b+"?user="+c+"&password="+d+""
		String url = "jdbc:sqlserver://172.16.94.48:1433;databaseName=LiTT"

		if(connection != null && !connection.isClosed()){
			connection.close()
		}
		connection = DriverManager.getConnection(url)
		return connection
	}

	@Keyword
	def executeQuery(String queryString) {
		Statement stm = connection.createStatement()
		ResultSet rs = stm.executeQuery(queryString)
		return rs
	}

	@Keyword
	def closeDatabaseConnection() {
		if(connection != null && !connection.isClosed()){
			connection.close()
		}
		connection = null
	}

	@Keyword
	def executeFlagLK(String LK) {
		String queryString = 'UPDATE "OTOSURVEY_LK SET" bIsRun_LK = "1" WHERE "ID_LK" = '+LK+''
		Statement stm = connection.createStatement()
		boolean result = stm.execute(queryString)
		return result
	}
}
