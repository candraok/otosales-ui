package general

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory

import io.appium.java_client.AppiumDriver

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.text.NumberFormat
import java.util.Locale;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class master {

	@Keyword

	def getDateToday(String format){
		//ex 'hh:mm:ss a' will return 12:59:59 AM
		Date date = new Date()
		DateFormat dateFormat = new SimpleDateFormat(format)
		String formattedDate= dateFormat.format(date)
	}

	@Keyword

	def removeCurrency(String money){
		money = money.replace(',', '')
		money = money.replace('Rp', '')
		money = money.trim()
		return money
	}

	@Keyword

	def removeTrailingZeros(String money){
		money = money.replace('.0000', '')
		money = money.trim()
		return money
	}

	@Keyword

	def listToString(ArrayList data, String divider){
		String result = ''
		for(int i = 0; i< data.size();i++){
			result+=data[i] + divider + ''
		}
		return result.substring(0, result.length()-divider.length())
	}

	@Keyword

	def readStickerFile(String filePath) {
		BufferedReader br = null
		String line = ""
		List<String> stickerList = new ArrayList<>()
		List<String> result = new ArrayList<>()
		try {
			br = new BufferedReader(new FileReader(filePath));
			line = br.readLine()
			while ((line = br.readLine()) != null) {
				stickerList.add(line)
			}
			result.add(stickerList[0])
			result.add(stickerList[stickerList.size()-1])
		}
		catch(Exception ex){
			String what = ex.message
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return result
		}
	}

	@Keyword

	def readStickerFileRange(String filePath, int startFrom, int end) {
		BufferedReader br = null
		String line = ""
		List<String> stickerList = new ArrayList<>()
		List<String> result = new ArrayList<>()
		try {
			br = new BufferedReader(new FileReader(filePath));
			line = br.readLine()
			while ((line = br.readLine()) != null) {
				stickerList.add(line)
			}
			for(int i = startFrom; i< end; i++){
				result.add(stickerList[i])
			}

			//result.add(stickerList[stickerList.size()-1])
		}
		catch(Exception ex){
			String what = ex.message
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return result
		}
	}

	@Keyword

	def getLatestFilePath(){
		File fl = new File(GlobalVariable.DownloadFolder);
		File[] files = fl.listFiles(new FileFilter() {
					public boolean accept(File file) {
						return file.isFile();
					}
				});
		long lastMod = Long.MIN_VALUE;
		File choice = null;
		for (File file : files) {
			if (file.lastModified() > lastMod) {
				choice = file;
				lastMod = file.lastModified();
			}
		}
		return choice.path;
	}
}
